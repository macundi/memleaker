//
//  DummyObject.swift
//  MemoryLeaker
//
//  Created by Markus Stöbe on 01.12.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

import Cocoa

class DummyObject: NSObject {
	
	public var previous: DummyObject?
	public var next: DummyObject?
	public var data: Data?
	
	public class func buildRing(size:Int) -> DummyObject {
		var firstDummy: DummyObject?
		var lastDummy : DummyObject?
		
		for i in 0..<size {
			//create new object
			let newDummy = DummyObject()
			
			//set pointers to next and previous object
			newDummy.previous = lastDummy
			lastDummy?.next   = newDummy
			
			//save pointer to first object
			if (i == 0) {
				firstDummy = newDummy
			}
			
			//save pointer to newObject for next cycle
			lastDummy = newDummy
		}
		
		//connect first and last dummy to build the ring
		firstDummy?.previous = lastDummy
		lastDummy?.next = firstDummy
		
		return firstDummy!
	}
	
}
