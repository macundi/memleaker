//
//  ViewController.swift
//  MemoryLeaker
//
//  Created by Markus Stöbe on 01.12.16.
//  Copyright © 2016 Markus Stöbe. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

	var listOfObjects: DummyObject?
	
	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
		listOfObjects = DummyObject.buildRing(size:5);
	}

	override var representedObject: Any? {
		didSet {
		// Update the view, if already loaded.
		}
	}

	@IBAction func wasteSomeMemory(_ sender: Any) {
		listOfObjects = nil
	}

}

